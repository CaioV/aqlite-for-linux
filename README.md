# AQLite for linux!

I am tired of having problems because Adobe decided to shut down flash. So for now, I created this.

To use it, have installed everything Pepper flash normally needs
(in Arch Linux's case, just the gcc-libs package), and run it. If you clone the repository and want to run in the folder itself, run:

- npm install electron@4.2.12

To run the program itself, run "npm start"

Before running the program, load the macromedia website, pressing Alt+O, and do as explained in Spider's guide to run in firefox, and it's necessary here too (for now. Plans to remove this part in progress).

Finally, make it load aqlite.swf, and go play your game. Oh, try to hit F1 on the program. Something fun will reveal itself.

- THIS AQLITE.SWF IS BETA 15.2!

## But I Want a package. How to compile one for myself?

Download it from the drive link

https://drive.google.com/drive/folders/1Qwk4F3StAxdQD0BzRk4s7XZ6565no6eX?usp=sharing

But you can compile it if you want it.

 - Install with sudo the electron packager using npm (root permission only here in the installation) 
 - Run on the project folder the packager.
 - The new folder generated should be a standalone exe. Note that may be necessary to go to the macromedia website using electron before to work. (In version 1.1 and above of this program, just press Alt+O)
 
The commands for the above instructions:
 
 - sudo npm install electron-packager -g
 - electron-packager ./ AQLite-for-linux
 
## Notes:

- Electron is in an older version, because newer ones don't like to run flash.
- This is just so I can still play my game. So this is licensed MIT.

- NOTE: The flash file is LGPL + custom license. Go get yours from Adobe/your repository as it shold be.
