const {app, BrowserWindow, globalShortcut} = require('electron');
const path = require('path');

let mainWindow;

app.on('window-all-closed', function() {
  if (process.platform != 'darwin')
    app.quit();
});

let ppapi_flash_path;

// Specify flash path.
// On Windows, it might be /path/to/pepflashplayer.dll
// On OS X, /path/to/PepperFlashPlayer.plugin
// On Linux, /path/to/libpepflashplayer.so
if(process.platform  == 'win32'){
    switch (os.arch()) {
        case 'x64':
            ppapi_flash_path = path.join(__dirname, 'flashdll', 'pepflashplayer64.dll');
            break
        default:
            ppapi_flash_path = path.join(__dirname, 'flashdll', 'pepflashplayer.dll');
            break
    }
} else if (process.platform == 'linux') {
    ppapi_flash_path = path.join(__dirname, 'flashdll', 'libpepflashplayer.so');
} else if (process.platform == 'darwin') {
    ppapi_flash_path = path.join(__dirname, 'flashdll', 'PepperFlashPlayer.plugin');
}
app.commandLine.appendSwitch('ppapi-flash-path', ppapi_flash_path);

// Specify flash version, for example, v18.0.0.203
//app.commandLine.appendSwitch('ppapi-flash-version', '18.0.0.203');

/// LINKS
aqlite_path = "file://" + path.join(__dirname, 'aqlite.swf');
flash_settings_web_path = 'https://www.macromedia.com/support/documentation/en/flashplayer/help/settings_manager04.html';

wiki_path = 'http://aqwwiki.wikidot.com/new-releases'
account_aq_path = 'https://account.aq.com/'
design_notes_path = 'https://www.aq.com/gamedesignnotes/'
character_lookup_path = 'https://www.aq.com/character.asp'

icon_path = path.join(__dirname, 'icon.png');

// New page function
function newBrowserWindow(mainWindow, new_path){
    if (mainWindow.isFocused()){
        const win = new BrowserWindow({
            'width': 1000,
            'height': 700,
            'webPreferences': {
                'plugins': true,
                'nodeIntegration': false,
                'javascript': true,
                'contextIsolation': true,
                'enableRemoteModule': false
            },
            'icon': icon_path,
        });
        win.loadURL(new_path);
    }
}

// Help Function
function showHelpMessage(){
    const { dialog } = require('electron')
    const dialog_options = {
        buttons: ['Ok'],
        title: 'Help:',
        message: "These are the keybindings added to the game. Note that they use 'Alt' with it",
        detail: 'W - AQW Wiki\n' +
            'D - AQW Design notes\n' +
            'A - Account page\n' + 
            'C - Character lookup. You can also just use the in-game lookup.\n' + 
            'O - Flash settings. Usefull to add the aqlite path to trusted folders.\n\n' +
            'Note: F1, or Cmd/Ctrl + H, or Alt + H Shows this message.',
    };
    const response = dialog.showMessageBox(null,dialog_options);
}

// Load AQLite
app.on('ready', function() {
    mainWindow = new BrowserWindow({
        'width': 1000,
        'height': 700,
        'title': 'AQLite',
        'webPreferences': {
            'plugins': true,
            'nodeIntegration': false,
            'javascript': true,
            'contextIsolation': true,
            'enableRemoteModule': false
        },
        'icon': icon_path
    });
    mainWindow.loadURL(aqlite_path);

    // KEYBINDINGS
    // ALT W for wiki
    const ret1 = globalShortcut.register('Alt+W',() => {
        newBrowserWindow(mainWindow,wiki_path);
    })
    const ret2 = globalShortcut.register('Alt+D',() => {
        newBrowserWindow(mainWindow,design_notes_path);
    })
    const ret3 = globalShortcut.register('Alt+A',() => {
        newBrowserWindow(mainWindow,account_aq_path);
    })
    const ret4 = globalShortcut.register('Alt+C',() => {
        newBrowserWindow(mainWindow,character_lookup_path);
    })
    const ret5 = globalShortcut.register('Alt+O',() => {
        newBrowserWindow(mainWindow,flash_settings_web_path);
    })
    
    // HELP MEEE
    const ret6 = globalShortcut.register('Alt+H',() => {
        showHelpMessage();
    })
    const ret7 = globalShortcut.register('CommandOrControl+H',() => {
        showHelpMessage();
    })
    const ret8 = globalShortcut.register('F1',() => {
        showHelpMessage();
    })

    
});
